library scale_size;

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// A class to help you scale your design on bigger or smaller screens to achieve the same design look.
class ScaleSize {
  /// Get status bar height
  static double statusBarHeight = 0;

  /// Get navigation bar height
  static double navigationBarHeight = 0;

  /// Get screen width
  static double screenWidth = 0;

  /// Get screen height
  static double screenHeight = 0;
  static double _designWidth = 0;
  static double _designHeight = 0;

  /// Setup the screen with a [context] and the [designWidth] (and the [designHeight]) you will use.
  /// So, if you have a design width = 360, height = 640. You will pass first the context
  /// then the designWidth (and designHeight).
  /// init(context, designWidth = 360, designHeight = 640)
  static void init(BuildContext context, {double designWidth = 0, double designHeight = 0}) {
    if (screenWidth > 0) {
      return;
    }
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    statusBarHeight = mediaQueryData.padding.top;
    navigationBarHeight = mediaQueryData.padding.bottom;
    screenWidth = mediaQueryData.size.width;
    screenHeight = mediaQueryData.size.height;
    if (designWidth <= 0) {
      _designWidth = screenWidth;
    } else {
      _designWidth = designWidth;
    }
    if (designHeight <= 0) {
      _designHeight = screenHeight;
    } else {
      _designHeight = designHeight;
    }
  }

  /// Setup the screen with a [context] and the [designWidth] (and the [designHeight]) you will use.
  /// So, you want reinit _designHeight , _designHeight when rotate screen
  static void reInit(BuildContext context,
      {double designWidth = 0, double designHeight = 0, Orientation orientation = Orientation.portrait}) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    statusBarHeight = mediaQueryData.padding.top;
    navigationBarHeight = mediaQueryData.padding.bottom;
    var width = mediaQueryData.size.width;
    var height = mediaQueryData.size.height;
    if (width > height) {
      var temp = width;
      width = height;
      height = temp;
    }
    if (orientation == Orientation.landscape) {
      screenWidth = height;
      screenHeight = width;
    } else {
      screenWidth = width;
      screenHeight = height;
    }
    if (designWidth <= 0) {
      _designWidth = screenWidth;
    } else {
      _designWidth = designWidth;
    }
    if (designHeight <= 0) {
      _designHeight = screenHeight;
    } else {
      _designHeight = designHeight;
    }
  }

  /// Get the number scaled vertically.
  /// If you want change current designWidth, you will pass [size] and [designWidth].
  /// scaleW(size, designWidth = 360)
  static double scaleW(double size, {double designWidth = 0}) {
    if (designWidth <= 0 && _designWidth == 0) {
      return size;
    }
    return size * screenWidth / _designWidth;
  }

  /// Get the number scaled horizontally.
  /// If you want change current designWidth, you will pass [size] and [designHeight].
  /// scaleH(size, designHeight = 360)
  static double scaleH(double size, {double designHeight = 0}) {
    if (designHeight <= 0 && _designHeight == 0) {
      return size;
    }
    return size * screenHeight / _designHeight;
  }
}

extension ConvertDouble on double {
  // scale by width
  double get sw {
    return ScaleSize.scaleW(this);
  }

  // scale by height
  double get sh {
    return ScaleSize.scaleH(this);
  }

  // get screen width
  double get width {
    return this * ScaleSize.screenWidth;
  }

  // get screen height
  double get height {
    return this * ScaleSize.screenHeight;
  }

  // get top padding (status bar height)
  double get top {
    return this * ScaleSize.statusBarHeight;
  }

  // get bottom padding (navigation bar height)
  double get bottom {
    return this * ScaleSize.navigationBarHeight;
  }
}

extension ConvertInt on int {
  // scale by width
  double get sw {
    return ScaleSize.scaleW(this.toDouble());
  }

  // scale by height
  double get sh {
    return ScaleSize.scaleH(this.toDouble());
  }

  // get screen width
  double get width {
    return this * ScaleSize.screenWidth;
  }

  // get screen height
  double get height {
    return this * ScaleSize.screenHeight;
  }

  // get top padding (status bar height)
  double get top {
    return this * ScaleSize.statusBarHeight;
  }

  // get bottom padding (navigation bar height)
  double get bottom {
    return this * ScaleSize.navigationBarHeight;
  }
}
