## [0.0.1] - 15/01/2021

* Initial release

## [0.0.2] - 15/01/2021

* If not call init first, the ScaleSize.get function will return size param

## [0.0.3] - 30/03/2021

* Add 2 extensions for the simple way use:
- Before:
```dart
ScaleSize.get(10);
```
- Current:
```dart
10.s
```

## [0.0.4] - 10/04/2021

* Support scale for size of height
* Add more extensions and change method name
- Before:
```dart
ScaleSize.get(10); // or 10.s
```
- Current:
```dart
ScaleSize.scaleW(10); // or 10.sw
ScaleSize.scaleH(10); // or 10.sh
```

## [0.0.5] - 13/08/2021
Now, you can re-init scale with function
```dart
ScaleSize.reInit(context, designWidth: 360, designHeight: 640, orientation: Orientation.portrait);
```

## [0.0.6] - 05/10/2021
* Null safety
