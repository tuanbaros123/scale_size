# Scale Size

[![pub package](https://img.shields.io/pub/v/scale_size.svg)](https://pub.dev/packages/scale_size)

A Flutter package helps applications design differently from GUI guides, even on devices with different resolutions.

## Usage
To use this plugin, add `scale_size` as a [dependency in your pubspec.yaml file](https://flutter.dev/docs/development/packages-and-plugins/using-packages).

### Example

``` dart
// Import package
import 'package:scale_size/scale_size.dart';

// Instantiate it in build function of first screen (ex: Splash screen)
class SplashScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // designWidth, designHeight is size of width, height in GUI design (ex: figma, zeplin, ...)  
    ScaleSize.init(context, designWidth: 360, designHeight: 640);
    return Scaffold(...);
  }
}

// Access size with init designWidth
ScaleSize.scaleW(10); or 10.sw
// Access size with current designWidth
ScaleSize.scaleW(10, designWidth: 360);

// Access size with init designHeight
ScaleSize.scaleH(10); or 10.sh
// Access size with current designHeight
ScaleSize.scaleH(10, designHeight: 640);

// Available size after init
ScaleSize.statusBarHeight; // or 1.top
ScaleSize.navigationBarHeight; // or 1.bottom
ScaleSize.screenWidth; // or 1.width
ScaleSize.screenHeight}; // or 1.height

// Re-init
ScaleSize.reInit(context, designWidth: 360, designHeight: 640, orientation: Orientation.portrait);
```